#include <iostream>

using namespace std;

int countDeterminant(int** A) {

}


void printMatrix(int** A, int size) {
    int i = 0, j = 0;
//    int size = ;//sizeof(**A) + 1;//sizeof(A); //особенность выполнения функции
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            std::cout << A[i][j] << " | ";
        }
        std::cout << "\n";
    }
}

int** initMatrix(int size) {
    srand(5);
    int i = 0, j = 0;
    int **matrix;
    matrix = new int*[size];
    for (i = 0; i < size; i++) {
        matrix[i] = new int[size];
        for (j = 0; j < size; j++) {
            matrix[i][j] = (int) (rand() % 5 +2);
        }
    }
    return matrix;
}

void GetMatr(int **mas, int **p, int i, int j, int m) {
    int ki, kj, di, dj;
    di = 0;
    for (ki = 0; ki<m - 1; ki++) { // проверка индекса строки
        if (ki == i) di = 1;
        dj = 0;
        for (kj = 0; kj<m - 1; kj++) { // проверка индекса столбца
            if (kj == j) dj = 1;
            p[ki][kj] = mas[ki + di][kj + dj];
        }
    }
}

int Determinant(int **mas, int m) {
    int i, j, d, k, n;
    int **p;
    p = new int*[m];
    for (i = 0; i<m; i++)
        p[i] = new int[m];
    j = 0; d = 0;
    k = 1; //(-1) в степени i
    n = m - 1;
    if (m<1) cout << "Определитель вычислить невозможно!";
    if (m == 1) {
        d = mas[0][0];
        return(d);
    }
    if (m == 2) {
        d = mas[0][0] * mas[1][1] - (mas[1][0] * mas[0][1]);
        return(d);
    }
    if (m>2) {
        for (i = 0; i<m; i++) {
            GetMatr(mas, p, i, 0, m);
//            cout << mas[i][j] << endl;
//            PrintMatr(p, n);
            d = d + k * mas[i][0] * Determinant(p, n);
            k = -k;
        }
    }
    return(d);
}

int main() {
    int size = 3;
    int **matrix = initMatrix(size);
    printMatrix(matrix, size);
    cout << "determinant = " << Determinant(matrix, size);
    return 0;
}


